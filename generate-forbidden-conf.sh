#!/bin/bash
      echo "countries map \$http_cf_ipcountry \$allow {" > 403countries.conf
      echo "  default yes;" >> 403countries.conf
      IFS=',' read -ra COUNTRIES_ARRAY <<< "$BLOCKED_COUNTRIES"
      for COUNTRY_CODE in "${COUNTRIES_ARRAY[@]}"; do
          echo "  $COUNTRY_CODE no;" >> 403countries.conf
      done

      echo "}" >> 403countries.conf
